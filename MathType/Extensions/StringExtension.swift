import UIKit

extension String {
    var localized: String {
        let lang = Language.getLang()
        let path: String! = Bundle.main.path(forResource: lang, ofType: "lproj")!
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
