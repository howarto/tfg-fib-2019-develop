import UIKit

private let OPTIONS_IDENTIFIER = "ConfigurationOptionCell"

class ConfigurationView: ViewBase {
    // MARK: - Properties
    
    var delegate: ConfigurationViewDelegate?
    
    private var backgroundEffect: UIVisualEffectView!
    private var settingsContainer: UIView!
    private var effectRef: UIVisualEffect!
    private var titleLabel: UILabel!
    private var tableView: UITableView!
    private var confirmationButton: UIButton!
    private var configurationCells: [UIView]!
    
    private var needsRefresh: Bool!
    private var smallerWidthConstraint: NSLayoutConstraint?
    private var biggerWidthConstraint: NSLayoutConstraint?
    private var smallerHeightConstraint: NSLayoutConstraint?
    private var biggerHeightConstraint: NSLayoutConstraint?

    
    // MARK: - Functions
    
    override func render() {
        // Initialize properties
        needsRefresh = false
        
        // Add all the layers that compose the view.
        
        // Background effect.
        createBackgroundEffect()
        
        // Settings container.
        createSettingsContainer()
        
        animateIn()
    }
    
    private func createBackgroundEffect() {
        // Blur effect.
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        backgroundEffect = UIVisualEffectView(effect: blurEffect)
        // Keep the effect and turn off.
        effectRef = backgroundEffect.effect
        backgroundEffect.effect = nil
        
        view.addSubview(backgroundEffect)
        backgroundEffect.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backgroundEffect.heightAnchor.constraint(equalTo: view.heightAnchor),
            backgroundEffect.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
    }
    
    override func refreshSize() {
        super.refreshSize()
        toggleWidthConstraints()
        toggleHeightConstraints()
    }
    
    private func toggleWidthConstraints() {
        if settingsContainer != nil {
            if view.bounds.width <= 400 {
                biggerWidthConstraint?.isActive = false
                smallerWidthConstraint?.isActive = true
            }
            else {
                smallerWidthConstraint?.isActive = false
                biggerWidthConstraint?.isActive = true
            }
        }
    }
    
    private func toggleHeightConstraints() {
        if settingsContainer != nil {
            if view.bounds.width <= 700 {
                biggerHeightConstraint?.isActive = false
                smallerHeightConstraint?.isActive = true
            }
            else {
                smallerHeightConstraint?.isActive = false
                biggerHeightConstraint?.isActive = true
            }
        }
    }
    
    private func createSettingsContainer() {
        // Init view.
        settingsContainer = UIView()
        settingsContainer.backgroundColor = ColorPicker.white.uiColor
        settingsContainer.alpha = 0.8
        settingsContainer.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        settingsContainer.layer.cornerRadius = 5

        // Add title label.
        addTitleLabel()
        // Add table view.
        addTablewView()
        // Add confirmation button.
        addConfirmationButton()
        
        view.addSubview(settingsContainer)
        settingsContainer.translatesAutoresizingMaskIntoConstraints = false
        
        smallerWidthConstraint = settingsContainer.widthAnchor.constraint(greaterThanOrEqualToConstant: 300)
        biggerWidthConstraint = settingsContainer.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5)
        
        toggleWidthConstraints()
        
        smallerHeightConstraint = settingsContainer.heightAnchor.constraint(equalTo: settingsContainer.widthAnchor, multiplier: 1)
        biggerHeightConstraint = settingsContainer.heightAnchor.constraint(equalTo: settingsContainer.widthAnchor, multiplier: 0.45)
        
        toggleHeightConstraints()

        if Device.isPad {
            NSLayoutConstraint.activate([
                settingsContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                settingsContainer.centerYAnchor.constraint(equalTo: view.centerYAnchor)
                ])
        }
        else {
            NSLayoutConstraint.activate([
                settingsContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                settingsContainer.centerYAnchor.constraint(equalTo: view.centerYAnchor)
                ])
        }
    }
    
    
    /// Adds the title label to the settingsContainer element.
    private func addTitleLabel() {
        titleLabel = UILabel()
        titleLabel.text = "Settings".localized
        titleLabel.textColor = ColorPicker.black.uiColor
        titleLabel.font = UIFont.systemFont(ofSize: 22)
        titleLabel.textAlignment = .center
        
        settingsContainer.addSubview(titleLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: settingsContainer.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: settingsContainer.rightAnchor),
            titleLabel.topAnchor.constraint(equalTo: settingsContainer.topAnchor, constant: 20),
        ])
    }
    
    /// Adds the table view to the settingsContainer element.
    private func addTablewView() {
        tableView = UITableView()
        tableView.delegate = self; 
        tableView.dataSource = self;
        
        tableView.register(ConfigurationOptionCell.self, forCellReuseIdentifier: OPTIONS_IDENTIFIER)
        
        settingsContainer.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: settingsContainer.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: settingsContainer.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        
        tableView.separatorStyle = .none
        tableView.rowHeight = 80
    }

    /// Removes settings container.
    private func removeSettingsContainer() {
        settingsContainer.removeFromSuperview()
        settingsContainer = nil
        titleLabel = nil
        tableView = nil
        confirmationButton = nil
    }
    
    /// Adds the confirmation button to the settingsContainer element.
    private func addConfirmationButton() {
        confirmationButton = UIButton()
        confirmationButton.setTitle("OK".localized, for: UIControl.State.normal)
        confirmationButton.setTitleColor(ColorPicker.blue.uiColor, for: UIControl.State.normal)
        confirmationButton.setTitleColor(ColorPicker.cyan.uiColor, for: UIControl.State.highlighted)
        confirmationButton.addTarget(self, action: #selector(confirmationButtonPressed), for: .touchUpInside)
        
        settingsContainer.addSubview(confirmationButton)
        
        confirmationButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            confirmationButton.leftAnchor.constraint(equalTo: settingsContainer.leftAnchor),
            confirmationButton.rightAnchor.constraint(equalTo: settingsContainer.rightAnchor),
            confirmationButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            confirmationButton.bottomAnchor.constraint(equalTo: settingsContainer.bottomAnchor)
        ])
    }
    
    override func animateIn() {
        // Needs refhesh each time that it is shown.
        if needsRefresh {
            createSettingsContainer()
        }
        
        UIView.animate(withDuration: 0.2) {
            // Due to white color predomines on the background,
            // is better having a low alpha to look more the blur effect.
            self.view.alpha = 1
            // Add blur effect.
            self.backgroundEffect.effect = self.effectRef
            // Remove previous transforms.
            self.settingsContainer.transform = CGAffineTransform.identity
        }
    }
    
    override func animateOut() {
        UIView.animate(withDuration: 0.2) {
            // Due to white color predomines on the background,
            // is better having a low alpha to look more the blur effect.
            self.view.alpha = 0
            // Add blur effect.
            self.backgroundEffect.effect = self.effectRef
            // Remove previous transforms.
            self.settingsContainer.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        }
        removeSettingsContainer()
    }
}

extension ConfigurationView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ConfigurationOption.allCases.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OPTIONS_IDENTIFIER, for: indexPath) as! ConfigurationOptionCell
        let configurationOption = ConfigurationOption.allCases[indexPath.row]

        cell.titleText.text = configurationOption.text
        
        let selection = configurationOption.selection
        
        switch configurationOption {
        case .Language:
            let langSelection = selection as! LanguageUIPickerView
            langSelection.delegate = self
        case .Toolbar:
            let toolbarSelection = selection as! ToolbarUIPickerView
            toolbarSelection.delegate = self
        }
        
        let uiView = selection.view!
        cell.chooseElement.addSubview(uiView)
        uiView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            uiView.leftAnchor.constraint(equalTo: cell.chooseElement.leftAnchor),
            uiView.rightAnchor.constraint(equalTo: cell.chooseElement.rightAnchor),
            uiView.topAnchor.constraint(equalTo: cell.chooseElement.topAnchor),
            uiView.bottomAnchor.constraint(equalTo: cell.chooseElement.bottomAnchor),
            uiView.heightAnchor.constraint(equalTo: cell.chooseElement.heightAnchor)
            ])
        
        return cell
    }
    
    // Mark: - Handlers
    
    /// Handler for confirmation button pressed.
    @objc private func confirmationButtonPressed() {
        delegate?.onSelectedLanguage()
        delegate?.onAcceptConfiguration()
        needsRefresh = true
    }
}

extension ConfigurationView: LanguageUIPickerViewDelegate {
    func onSelectLanguage() {
        // Nothing.
    }
}

extension ConfigurationView: ToolbarUIPickerViewDelegate {
    var currentEditorToolbar: ToolbarOption {
        return delegate!.getEditorToolbar()
    }
    
    func onSelectedToolbar(_ toolbarOption: ToolbarOption) {
        delegate?.onSelectedToolbar(toolbarOption)
    }
}
