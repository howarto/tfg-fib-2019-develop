protocol JavascriptCommunicationProtocol {
    /// Inserts a javascript file into the current context.
    ///
    /// - Parameter fileName: Name of the file to insert and that it's inside the XCode project.
    func insertJavascriptFile(fileName: String)
    
    /// Creates and initializes an editor instance inside
    /// the current Javascript context.
    func initEditor()
    
    /// Calls the recognizer image service to try to get the
    /// corresponding mathml of the image.
    ///
    /// - Returns: Empty String or the MathML of the image.
    func getMathMLFromPng() -> String
    
    /// Sets mathml inside the editor instance of the current
    /// Javascript context.
    ///
    /// - Parameter mathml: mathml to insert inside the editor.
    func setMathML(mathml: String)
    
    /// Gets the current formula inside the editor.
    func getCurrentFormula()
    
    /// Sets specified toolbar in editor instance.
    ///
    /// - Parameter toolbarOption: Chosen toolbar to set in editor instance.
    func setToolbar(_ toolbarOption: ToolbarOption)
    
    /// Recognizes the image and put the result inside the editor.
    ///
    /// - Parameter base64: Base64 string from an image.
    func recognizeImage(with base64: String)
}
