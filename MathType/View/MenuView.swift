import UIKit

private let RECENTFORMULAS_ROWS_NUMBER = 5
private let OPTIONS_IDENTIFIER = "OptionCell"
private let RECENTFORMULAS_IDENTIFIER = "RecentFormulaCell"


/// View of the menu where user can select recent formulas, take a photo
/// to analyze or change the default configuration.
class MenuView: ViewBase {
    // MARK: - Properties
    
    private var optionsTableView: UITableView!
    private var recentFormulasTableView: UITableView!
    private var recentFormulasLabel: UILabel!
    private var optionsTableViewDelegate: OptionsTableViewDelegate!
    private var recentFormulasTableViewDelegate: RecentFormulasTableViewDelegate!

    var delegate: MenuViewDelegate?
    
    // MARK: - Functions
    
    override func render() {
        view.backgroundColor = ColorPicker.lightRedWiris.uiColor
        configureTableView()
    }
    
    func onSelectedLanguage() {
        optionsTableView.reloadData()
        recentFormulasTableView.reloadData()
        recentFormulasLabel.text = "Recent formulas".localized
    }
    
    private func configureTableView() {
        // Options tableview.
        optionsTableView = UITableView()
        
        optionsTableView.isScrollEnabled = false;
        optionsTableView.backgroundColor = ColorPicker.lightRedWiris.uiColor
        optionsTableView.separatorStyle = .none
        optionsTableView.rowHeight = 80

        optionsTableViewDelegate = OptionsTableViewDelegate()
        optionsTableViewDelegate.delegate = delegate
        optionsTableView.delegate = optionsTableViewDelegate
        optionsTableView.dataSource = optionsTableViewDelegate

        optionsTableView.register(MenuOptionCell.self, forCellReuseIdentifier: OPTIONS_IDENTIFIER)

        view.addSubview(optionsTableView)
        optionsTableView.translatesAutoresizingMaskIntoConstraints = false
        optionsTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        optionsTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        optionsTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        optionsTableView.heightAnchor.constraint(equalToConstant: optionsTableView.rowHeight * CGFloat(MenuOption.allCases.count)).isActive = true

        // Recent formulas label.
        recentFormulasLabel = UILabel()
        recentFormulasLabel.text = "Recent formulas".localized
        recentFormulasLabel.textColor = ColorPicker.white.uiColor
        recentFormulasLabel.font = UIFont.systemFont(ofSize: 22)
        
        view.addSubview(recentFormulasLabel)
        
        recentFormulasLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            recentFormulasLabel.leftAnchor.constraint(equalTo: view.leftAnchor),
            recentFormulasLabel.rightAnchor.constraint(equalTo: view.rightAnchor),
            recentFormulasLabel.topAnchor.constraint(equalTo: optionsTableView.bottomAnchor, constant: 30)
            ])

        // Recent formulas tableview.
        recentFormulasTableView = UITableView()
        recentFormulasTableView.backgroundColor = ColorPicker.lightRedWiris.uiColor
        recentFormulasTableView.separatorStyle = .none
        recentFormulasTableView.rowHeight = 125
        recentFormulasTableViewDelegate = RecentFormulasTableViewDelegate()
        recentFormulasTableViewDelegate.delegate = delegate
        recentFormulasTableViewDelegate.tableView = recentFormulasTableView
        recentFormulasTableView.delegate = recentFormulasTableViewDelegate
        recentFormulasTableView.dragInteractionEnabled = true
        recentFormulasTableView.dragDelegate = recentFormulasTableViewDelegate
        recentFormulasTableView.dataSource = recentFormulasTableViewDelegate
        
        recentFormulasTableView.register(MenuRecentFormulaCell.self, forCellReuseIdentifier: RECENTFORMULAS_IDENTIFIER)
        
        
        view.addSubview(recentFormulasTableView)

        recentFormulasTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            recentFormulasTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            recentFormulasTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            recentFormulasTableView.topAnchor.constraint(equalTo: recentFormulasLabel.bottomAnchor, constant: 22),
            recentFormulasTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
    
    func addRecentFormula(formula: Formula) {
        recentFormulasTableViewDelegate.addRecentFormula(formula: formula)
        recentFormulasTableViewDelegate.update()
    }
}

class OptionsTableViewDelegate: ViewBase, UITableViewDelegate, UITableViewDataSource {
    var delegate: MenuViewDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOption.allCases.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OPTIONS_IDENTIFIER, for: indexPath) as! MenuOptionCell
        let menuOption = MenuOption.allCases[indexPath.row]
        cell.descriptionLabel.text = menuOption.description
        cell.iconImageView.image = menuOption.image

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = MenuOption.allCases[indexPath.row]
        delegate?.onOptionSelected(forMenuOption: menuOption)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

class RecentFormulasTableViewDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    private var recentImages = [Formula]()
    
    var delegate: MenuViewDelegate?
    var tableView: UITableView!
    
    // MARK: - Functions
    
    /// Adds a recent formula into the data.
    ///
    /// - Parameter formulaSrc: Src to an image. It must exist the resource.
    func addRecentFormula(formula: Formula) {
        let lastCurrentRecentFormula = recentImages.last
        if lastCurrentRecentFormula == nil {
            // The recentImages array is empty.
            recentImages.append(formula)
        }
        else if lastCurrentRecentFormula?.mml != formula.mml {
            // There was a recent formula before and it's not the same than the new recent formula to add.
            if recentImages.count == RECENTFORMULAS_ROWS_NUMBER {
                recentImages.removeFirst()
            }
            
            recentImages.append(formula)
        }
    }
    
    /// Reload tableview data.
    func update() {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentImages.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RECENTFORMULAS_IDENTIFIER, for: indexPath) as! MenuRecentFormulaCell
        
        let formulaUrl = getRecentFormulaWithReverseIndex(withIndex: indexPath.row).src
        let url = URL(string: formulaUrl!)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        cell.iconImageView.image = UIImage(data: data!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let formula = getRecentFormulaWithReverseIndex(withIndex: indexPath.row)
        delegate?.onRecentFormulaSelected(formula: formula)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /// Returns formula of the formula array beginning from the end.
    private func getRecentFormulaWithReverseIndex(withIndex index: Int) -> Formula {
        let count = recentImages.count
        return recentImages[count - 1 - index]
    }
}

extension RecentFormulasTableViewDelegate: UITableViewDragDelegate {
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let formulaUrl = getRecentFormulaWithReverseIndex(withIndex: indexPath.row).src
        let url = URL(string: formulaUrl!)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        let image = UIImage(data: data!)
        let item = NSItemProvider(object: image!)
        let dragItem = UIDragItem(itemProvider: item)
        return [dragItem]
    }
}
