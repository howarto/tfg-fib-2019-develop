protocol HomeViewDelegate {
    /// Instantiate if it's needed a BrowserController and returns an instance of its view.
    ///
    /// - Returns: Browser view as its superclass ViewBase.
    func getBrowserView() -> ViewBase
    
    /// Instantiate if it's needed a MenuController and returns an instance of its view.
    ///
    /// - Returns: Menu view as its superclass ViewBase.
    func getMenuView() -> ViewBase
    
    /// Instantiate if it's needed a ConfigurationController and returns an instance of its view.
    ///
    /// - Returns: Configuration view as its superclass ViewBase.
    func getConfigurationView() -> ViewBase
    
    /// Instantiate if it's needed a ImagePickerController and returns an instance of its view.
    ///
    /// - Returns: ImagePicker view as its superclass ViewBase.
    func getImagePickerView() -> ViewBase
}
