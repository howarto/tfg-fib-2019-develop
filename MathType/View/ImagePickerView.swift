import UIKit

class ImagePickerView: ViewBase {
    
    /// MARK: - Properties
    
    var delegate: ImagePickerViewDelegate!
    
    /// Picker to display.
    private var picker: UIImagePickerController!
    
    /// MARK: - Functions

    override func render() {
        picker = UIImagePickerController()
        picker.modalPresentationStyle = .overFullScreen
        
        picker.delegate = self
        picker.allowsEditing = true
        picker.mediaTypes = ["public.image"]
        
        present(from: self.view)
    }
    
    /// Creates an action consumed by UIAlert.
    ///
    /// - Parameters:
    ///   - type: Necessary type feature.
    ///   - title: Title of the action.
    /// - Returns: Returns an action with the specified type and title that executes the image picker.
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.picker.sourceType = type
            self.present(self.picker, animated: true)
        }
    }
    
    /// Present an alert to choose the method to take an image.
    ///
    /// - Parameter sourceView: View where create the alert.
    private func present(from sourceView: UIView) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (uiAlertAction) in
            self.delegate?.needRemove()
        }))

        // Uncomment if preferredStyle is actionSheet.
//        if Device.isPad {
//            alertController.popoverPresentationController?.sourceView = sourceView
//            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
//            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
//        }
        
        self.present(alertController, animated: true)
    }
    
    /// Function do remove ImagePicker view and let memory free.
    private func finishView() {
        picker.dismiss(animated: true, completion: {
            self.delegate?.needRemove()
        })
    }
}

extension ImagePickerView: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            finishView()
            return
        }
        let resizedImage = image.resizeWith(width: 400)
        delegate?.onImage(didSelect: resizedImage!)
        finishView()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        finishView()
    }
}

extension ImagePickerView: UINavigationControllerDelegate {
    // Empty.
}
