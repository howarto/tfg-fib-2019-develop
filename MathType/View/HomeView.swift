import UIKit

/// View of the homeview where user uses MathType Editor and he can access to a slide menu.
class HomeView: ViewBase {
    // MARK: - Properties
    
    /// Offset to slide when the menu is opened.
    private var SLIDE_OFFSET: CGFloat = 250
    /// Configuration view
    private var configurationView: ViewBase?
    /// Picker view
    private var imagePickerView: ViewBase?
    /// BrowserView to not get an instance each time.
    private var browserView: ViewBase!
    // MenuView to not get an instance each time.
    private var menuView: MenuView!
    /// Says if the side menu is opened or not.
    private var menuExpanded: Bool = false
    /// Button to copy editor formula.
    private var uiButton: CopyButton!
    private var uiButtonInitialOriginX: CGFloat!
    /// Activity indicator when something needs to be loaded.
    private var uiActivityIndicator: UIActivityIndicatorView?
    
    /// Controller of this class.
    var delegate: HomeViewDelegate!
    
    // MARK: - Preferred status bar styles.
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return menuExpanded
    }
    
    // MARK: - Functions
    
    override func render() {
        view.backgroundColor = ColorPicker.white.uiColor
        showNavigationBar()
        showBrowserView()
        showMenuView()
        showCopyButton()
    }
    
    /// Displays loading indicator.
    func showLoading() {
        if uiActivityIndicator == nil {
            uiActivityIndicator = UIActivityIndicatorView()
            uiActivityIndicator!.hidesWhenStopped = true
            uiActivityIndicator!.style = UIActivityIndicatorView.Style.gray
            // Scale more bigger.
            uiActivityIndicator!.transform = CGAffineTransform(scaleX: 2, y: 2)
            view.addSubview(uiActivityIndicator!)
            
            uiActivityIndicator!.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                uiActivityIndicator!.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                uiActivityIndicator!.centerYAnchor.constraint(equalTo: view.centerYAnchor)
                ])
        }
        
        uiActivityIndicator?.startAnimating()
    }
    
    /// Function called when user wants to recognize a formula in an image.
    func onRecognizeFormulaInImage() {
        if imagePickerView == nil {
            imagePickerView = delegate.getImagePickerView()
            addChild(imagePickerView!)
            view.addSubview(imagePickerView!.view)
            imagePickerView?.didMove(toParent: self)
            imagePickerView?.render()
        }
    }
    
    /// Actions to execute when image was taken to recognize from ImagePickerView.
    func didImagePicked() {
        imagePickerView?.view.removeFromSuperview()
        imagePickerView?.removeFromParent()
        imagePickerView = nil
    }
    
    /// Hides loading indicator.
    func hideLoading() {
        uiActivityIndicator?.stopAnimating()
    }
    
    /// Function called when browser view has an error
    func onErrorBrowserView(_ error: String, with message: String) {
        hideLoading()
        ToastView.long(view, message: message)
    }
    
    /// Creates a float button to copy current formula in Editor.
    private func showCopyButton() {
        let uiButtonWidthAndHeight = 60
        uiButton = CopyButton(frame: CGRect(x: 100, y: 100, width: uiButtonWidthAndHeight, height: uiButtonWidthAndHeight))
        // Hidden by default.
        uiButton.isHidden = true
        uiButton.backgroundColor = ColorPicker.lightRedWiris.uiColor
        uiButton.layer.cornerRadius = uiButton.frame.height / 2
        uiButton.layer.shadowRadius = 5
        uiButton.layer.shadowOpacity = 0.25
        uiButton.layer.shadowOffset = CGSize(width: 0, height: 10)
        
        uiButton.addTarget(self, action: #selector(copyFormula), for: .touchDown)
        
        view.addSubview(uiButton)
        
        uiButton.translatesAutoresizingMaskIntoConstraints = false
        
        if Device.isPad {
            NSLayoutConstraint.activate([
                uiButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40),
                uiButton.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -55),
                uiButton.heightAnchor.constraint(equalToConstant: CGFloat(uiButtonWidthAndHeight)),
                uiButton.widthAnchor.constraint(equalToConstant: CGFloat(uiButtonWidthAndHeight))
                ])
        }
        else {
            NSLayoutConstraint.activate([
                uiButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40),
                uiButton.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -30),
                uiButton.heightAnchor.constraint(equalToConstant: CGFloat(uiButtonWidthAndHeight)),
                uiButton.widthAnchor.constraint(equalToConstant: CGFloat(uiButtonWidthAndHeight))
                ])
        }
    }
    
    override func refreshSize() {
        configurationView?.refreshSize()
        browserView.refreshSize()
        super.refreshSize();
    }
    
    /// Function executed when the device is rotated. When it's rotated, menu is closed.
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menuExpanded = false
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            
            self.setNeedsStatusBarAppearanceUpdate()
            
        }, completion: { (completed) in
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                
                self.navigationController?.navigationBar.frame.origin.x = 0
                self.browserView.view.frame.origin.x = 0
                self.uiButton.transform = CGAffineTransform(translationX: 0, y: 0)
                
            }, completion: nil)
        })
    }
    
    /// Shows the float button.
    func showFloatButton() {
        uiButton.isHidden = false
    }
    
    /// Hides the float button.
    func hideFloatButton() {
        uiButton.isHidden = true
    }
    
    /// Shows configuration menu.
    func showConfigurationMenu() {
        if configurationView == nil {
            configurationView = delegate.getConfigurationView()
            // Put it over all the views.
            UIApplication.shared.keyWindow?.addSubview(configurationView!.view)
            configurationView?.render()
        }
        else {
            configurationView?.animateIn()
        }
    }
    
    /// Hides configuration menu.
    func hideConfigurationMenu() {
        configurationView?.animateOut()
    }
    
    /// Sets navigation bar properties and buttons.
    private func showNavigationBar() {
        navigationController?.navigationBar.barTintColor = ColorPicker.lightRedWiris.uiColor
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "MathType"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(HomeView.toogleMenu))
    }
    
    /// Adds the browser view that contains MathType Editor.
    private func showBrowserView() {
        if browserView == nil {
            browserView = delegate.getBrowserView()

            view.addSubview(browserView.view)
            addChild(browserView)
            browserView.didMove(toParent: self)

            browserView.render()
        }
    }
    
    /// Creates a MenuView instance and put it behind all the layers
    /// in 'appViewController'.
    ///
    /// - Parameter appViewController: view controller taken as a root.
    private func showMenuView() {
        if menuView == nil {
            menuView = delegate.getMenuView() as? MenuView
            
            view.insertSubview(menuView.view, at: 0)
            addChild(menuView)
            menuView.didMove(toParent: self)
            
            menuView.view.translatesAutoresizingMaskIntoConstraints = false
            menuView.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            menuView.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            menuView.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            menuView.view.widthAnchor.constraint(equalToConstant: SLIDE_OFFSET).isActive = true
            menuView.didMove(toParent: self)
            
            menuView.render()
        }
    }
    
    /// Closes menu.
    func closeMenu() {
        if menuExpanded {
            toogleMenu()
        }
    }
    
    // MARK: - Handlers
    
    /// Toogle menu.
    @objc func toogleMenu() {
        menuExpanded = !menuExpanded
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            
            self.setNeedsStatusBarAppearanceUpdate()
            
        }, completion: { (completed) in
            
            if self.menuExpanded {
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                    
                    let increase: CGFloat = self.SLIDE_OFFSET
                    self.browserView.view.frame.origin.x += increase
                    self.navigationController?.navigationBar.frame.origin.x += increase
                    self.uiButton.transform = CGAffineTransform(translationX: increase, y: 0)
                    
                }, completion: nil)
            }
            else {
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                    
                    self.navigationController?.navigationBar.frame.origin.x = 0
                    self.browserView.view.frame.origin.x = 0
                    self.uiButton.transform = CGAffineTransform(translationX: 0, y: 0)
                    
                }, completion: nil)
            }
        })
    }
    
    /// Handler when float button is pressed no get the current formula.
    @objc private func copyFormula() {
        uiButton.pulsate()
        let browserViewCast = browserView as! BrowserView
        browserViewCast.getCurrentFormula()
    }
}
