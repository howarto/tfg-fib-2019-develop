import UIKit

/// Base class from which all the views inherit.
class ViewBase: UIViewController {
    
    // MARK: - Functions
    
    /// Draw the view on screen.
    func render() {
        fatalError("Method had not been implemented")
    }
    
    // Resize to the actual screen size.
    func refreshSize() {}
    
    /// When it's called, the view is shown with an animation.
    func animateIn() {}
    
    /// When it's called, the view is hide with an animation.
    func animateOut() {}
}
