import UIKit

/// Application view. Initializes the application workflow.
class AppView: UIViewController {
    // MARK: - Properties
    
    private var actualController: UIViewController!
    private var homeView: ViewBase!
    override var childForStatusBarHidden: UIViewController? {
        return actualController
    }

    // MARK: - Init
    
    override func viewDidLayoutSubviews() {
        homeView.refreshSize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        view.translatesAutoresizingMaskIntoConstraints = false
        goToHome()
    }
    
    // MARK: - Functions

    private func goToHome() {
        let homeController = HomeController()
        homeView = homeController.getViewController()
        actualController = UINavigationController(rootViewController: homeView)
        
        view.addSubview(actualController.view)
        actualController.view.translatesAutoresizingMaskIntoConstraints = false
        actualController.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        actualController.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        actualController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        actualController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        addChild(actualController)
        actualController.didMove(toParent: self)
        
        homeView.render()
    }
}
