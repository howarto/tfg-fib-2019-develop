protocol ConfigurationViewDelegate {
    /// Executed actions after configuration is accepted.
    func onAcceptConfiguration()
    
    /// Executed actions after language change.
    func onSelectedLanguage()
    
    /// Executed actions after toolbar change.
    func onSelectedToolbar(_ toolbarOption: ToolbarOption)
    
    /// Gets current editor toolbar
    ///
    /// - Returns: Toolbar option of the current editor.
    func getEditorToolbar() -> ToolbarOption
}
