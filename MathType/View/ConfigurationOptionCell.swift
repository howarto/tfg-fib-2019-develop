import UIKit

class ConfigurationOptionCell: UITableViewCell {
    // MARK: - Properties
    
    var titleText: UILabel = {
        let label = UILabel()
        label.textColor = ColorPicker.black.uiColor
        label.font = UIFont.systemFont(ofSize: 22)
        label.text = "Undefined"
        return label
    }()
    
    var chooseElement: UIView! = UIView()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
                
        addSubview(titleText)
        titleText.translatesAutoresizingMaskIntoConstraints = false
        titleText.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleText.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        titleText.widthAnchor.constraint(equalToConstant: 110).isActive = true
        titleText.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        addSubview(chooseElement)
        chooseElement.translatesAutoresizingMaskIntoConstraints = false
        chooseElement.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        chooseElement.leftAnchor.constraint(equalTo: titleText.rightAnchor, constant: 15).isActive = true
        chooseElement.rightAnchor.constraint(equalTo: rightAnchor, constant: 15).isActive = true
        chooseElement.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
