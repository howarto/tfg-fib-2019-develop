protocol MenuViewDelegate {
    /// Function called when an option is chosen.
    ///
    /// - Parameter menuOption: Chosen option.
    func onOptionSelected(forMenuOption menuOption: MenuOption)
    
    /// Function called when a recent formula is selected.
    ///
    /// - Parameter formula: The selected formula.
    func onRecentFormulaSelected(formula: Formula)
}
