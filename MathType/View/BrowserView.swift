import WebKit

/// View that contains MathType Editor Web.
class BrowserView: ViewBase {
    // MARK: - Properties
    
    private let FORMULA_CHANGE_EVENT_NAME = "onRecentFormulasChange"
    private let FORMULA_GET_EVENT_NAME = "onGetFormula"
    private let FORMULA_GETMODE_EVENT_NAME = "onEditorModeChange"
    private let FORMULA_FINALIZELOAD_EVENT_NAME = "onFinalizeLoad"
    private let FORMULA_TOUCHSTART_EVENT_NAME = "onTouchStart"
    private let FORMULA_ERROR_EVENT_NAME = "onError"
    
    private var currentToolbar: ToolbarOption! = ToolbarOption.standard
    private var webView: WKWebView!
    
    public var delegate: BrowserViewDelegate?
    
    
    // MARK: - Functions
    
    /// Sets formula into the editor.
    func setFormula(formula: Formula) {
        setMathML(mathml: formula.mml)
    }
    
    /// Gets current toolbar internal name.
    func getEditorToolbar() -> ToolbarOption {
        return currentToolbar
    }
    
    override func render() {
        view.backgroundColor = ColorPicker.gray.uiColor
        // Configure webView message hnadler.
        let contentController = WKUserContentController();
        contentController.add(self, name: FORMULA_CHANGE_EVENT_NAME)
        contentController.add(self, name: FORMULA_GET_EVENT_NAME)
        contentController.add(self, name: FORMULA_GETMODE_EVENT_NAME)
        contentController.add(self, name: FORMULA_FINALIZELOAD_EVENT_NAME)
        contentController.add(self, name: FORMULA_TOUCHSTART_EVENT_NAME)
        contentController.add(self, name: FORMULA_ERROR_EVENT_NAME)

        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        // Expand webview.
        webView = WKWebView(frame: view.frame, configuration: config)
        webView.navigationDelegate = self
        webView.isUserInteractionEnabled = true
        
        // Configure interactions.
        // Disable webView interactions to catch the interaction in BrowserView.
        disableDragAndDropInteraction(webView)
        view.isUserInteractionEnabled = true
        let dropInteraction = UIDropInteraction(delegate: self)
        view.addInteraction(dropInteraction)

        // Add webview view.
        view.addSubview(webView)
        
        // Blank online website in order to avoid WIRIS security for online mode.
        webView.load(URLRequest(url: URL(string: "http://blank.org")!))
    }
    
    override func refreshSize() {
        webView.frame = view.bounds
        super.refreshSize()
    }
    
    // This script is a variation of the drag solution
    // found here: https://stackoverflow.com/questions/49911060/how-to-disable-ios-11-drag-drop-in-wkwebview .
    private func disableDragAndDropInteraction(_: WKWebView) {
        var webScrollView: UIView? = nil
        var contentView: UIView? = nil
        
        if #available(iOS 11.0, *) {
            guard let noDragWebView = webView else { return }
            webScrollView = noDragWebView.subviews.compactMap { $0 as? UIScrollView }.first
            contentView = webScrollView?.subviews.first(where: { $0.interactions.count > 1 })
            guard let dropInteraction = (contentView?.interactions.compactMap { $0 as? UIDropInteraction }.first) else { return }
            contentView?.removeInteraction(dropInteraction)
        }
    }
}

extension BrowserView: WKNavigationDelegate {
    /// Code that runs when the webview is loaded.
    ///
    /// - Parameters:
    ///   - webView: WebView instance.
    ///   - navigation: Navigation Controller instance of 'webView'.
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        initEditor()
    }
}

extension BrowserView: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == FORMULA_CHANGE_EVENT_NAME,
        let dict = message.body as? NSDictionary {
            let formulaImageSrc = dict["src"] as! String
            let formulaMathML = dict["mml"] as! String
            let newFormula = Formula(from: formulaImageSrc, withMathML: formulaMathML)
            delegate?.onNewRecentFormula(formula: newFormula)
        }
        else if message.name == FORMULA_GET_EVENT_NAME,
        let dict = message.body as? NSDictionary {
            let formulaImageSrc = dict["src"] as! String
            let formulaMathML = dict["mml"] as! String
            let currentFormula = Formula(from: formulaImageSrc, withMathML: formulaMathML)
            delegate?.onGetFormula(formula: currentFormula)
        }
        else if message.name == FORMULA_GETMODE_EVENT_NAME,
        let dict = message.body as? NSDictionary {
            let editorMode = dict["editorMode"] as! String
            delegate?.onEditorModeChange(editorMode: editorMode)
        }
        else if message.name == FORMULA_FINALIZELOAD_EVENT_NAME,
            let dict = message.body as? NSDictionary {
            let finalizeLoad = dict["finalizeLoad"] as! Bool
            if finalizeLoad {
                delegate?.onFinalizeLoad()                
            }
        }
        else if message.name == FORMULA_TOUCHSTART_EVENT_NAME,
            let dict = message.body as? NSDictionary {
            let touchStart = dict["touchStart"] as! Bool
            if touchStart {
                delegate?.onTouchStart()
            }
        }
        else if message.name == FORMULA_ERROR_EVENT_NAME,
            let dict = message.body as? NSDictionary {
            let error = dict["error"] as! String
            let message = dict["message"] as! String
            delegate?.onError(error, with: message)
        }
    }
}

extension BrowserView: UIDropInteractionDelegate {
    /// Returns true when the dropped object is an UIImage. Otherwise, false.
    ///
    /// - Parameters:
    ///   - interaction: The drop interaction instance.
    ///   - session: The sessions instance.
    /// - Returns: True in case that the dropped object is an UIImage. Otherwise, false.
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
    
    /// Choose the action option.
    ///
    /// - Parameters:
    ///   - interaction: The drop interaction instance.
    ///   - session: The sessions instance.
    /// - Returns: Drop proposal instance with the chosen action.
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }

    /// Action to do for the droped element.
    ///
    /// - Parameters:
    ///   - interaction: The drop interaction instance.
    ///   - session: The sessions instance.
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        session.loadObjects(ofClass: UIImage.self) { imageItems in
            let images = imageItems as! [UIImage]
            let base64 = images.first?.pngData()?.base64EncodedString()
            self.recognizeImage(with: base64!)
        }
    }
}

extension BrowserView: JavascriptCommunicationProtocol {
    func recognizeImage(with base64: String) {
        delegate?.onBeginToLoad()
        webView.evaluateJavaScript("var event = new CustomEvent('mathtypeRecognizeImage', { detail: '\(base64)' }); document.dispatchEvent(event)")
    }
    
    func setToolbar(_ toolbarOption: ToolbarOption) {
        currentToolbar = toolbarOption
        webView.evaluateJavaScript("editor.setParams({hand: true, 'toolbar': '\(toolbarOption.internalEditorName)'})", completionHandler: nil)
    }
    
    func insertJavascriptFile(fileName: String) {
        let javascriptUrl = Bundle.main.path(forResource: fileName, ofType: "js")!
        let javascript = try! String(contentsOfFile: javascriptUrl)
        webView.evaluateJavaScript(javascript)
    }
    
    func setMathML(mathml: String) {
        webView.evaluateJavaScript("editor.setMathML('\(mathml)')", completionHandler: nil)
    }
    
    func initEditor() {
        insertJavascriptFile(fileName: "InitEditor")
    }
    
    func getMathMLFromPng() -> String {
        // Stub.
        return "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mfenced><mfrac><mi>&#x3B1;</mi><mi>z</mi></mfrac></mfenced><mn>2</mn></msup></math>"
    }
    
    func getCurrentFormula() {
        webView.evaluateJavaScript("var event = new Event('mathtypeGetFormula'); document.dispatchEvent(event)")
    }
}
