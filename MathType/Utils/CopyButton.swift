import UIKit

class CopyButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let copyIconImage = UIImage(named: "icons8-copy-50-white")?.cgImage
        let copyIconImageScaled = UIImage(cgImage: copyIconImage!, scale: 1.6, orientation: UIImage.Orientation.up)
        setImage(copyIconImageScaled, for: UIControl.State.normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1
        
        layer.add(pulse, forKey: nil)
    }
}
