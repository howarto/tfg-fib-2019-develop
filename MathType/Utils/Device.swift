import UIKit

class Device {
    static var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}
