import UIKit

private func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

enum ColorPicker {
    case blue
    case cyan
    case red
    case orange
    case yellow
    case green
    case purple
    case pink
    case black
    case white
    case darkGray
    case gray
    case lightGray
    case lightRedWiris
    case redWiris
    case redOrangeWiris
    case lightOrangeWiris
    case orangeWiris
    
    var uiColor: UIColor {
        switch self {
        case .blue:
            return UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        case .cyan:
            return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 1)
        case .red:
            return UIColor(red: 255/255, green: 59/255, blue: 48/255, alpha: 1)
        case .orange:
            return UIColor(red: 255/255, green: 149/255, blue: 0/255, alpha: 1)
        case .yellow:
            return UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1)
        case .green:
            return UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        case .purple:
            return UIColor(red: 88/255, green: 86/255, blue: 214/255, alpha: 1)
        case .pink:
            return UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
        case .black:
            return UIColor.black
        case .white:
            return UIColor.white
        case .darkGray:
            return UIColor.darkGray
        case .gray:
            return UIColor.gray
        case .lightGray:
            return UIColor.lightGray
        case .lightRedWiris:
            return hexStringToUIColor(hex: "#DC4B5C")
        case .redWiris:
            return hexStringToUIColor(hex: "#B01630")
        case .redOrangeWiris:
            return hexStringToUIColor(hex: "#CE6854")
        case .lightOrangeWiris:
            return hexStringToUIColor(hex: "#F2D577")
        case .orangeWiris:
            return hexStringToUIColor(hex: "#F1B634")
        }
    }
}
