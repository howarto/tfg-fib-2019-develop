import UIKit

// SINGLETON class.
class Language {
    private static var currentLang: String = {
        return Locale.current.languageCode!
    }()
    
    static func setLang(_ lang: String) {
        currentLang = lang
    }
    
    static func getLang() -> String {
        return currentLang
    }
}
