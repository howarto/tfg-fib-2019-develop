import UIKit

// When ConfigurationOption is used. Pickerviews associated have weak references.
// That causes errors. With this array is possible transform them to strong references.
private var keepPickerviews: [UIViewController] = []

enum ConfigurationOption: CaseIterable {
    case Language
    case Toolbar
    
    var text: String {
        switch self {
        case .Language:
            return "Language".localized
        case .Toolbar:
            return "Toolbar".localized
        }
    }
    
    var selection: UIViewController {
        switch self {
        case .Language:
            let languageCaseUIPickerView = LanguageUIPickerView()
            keepPickerviews.append(languageCaseUIPickerView)
            return languageCaseUIPickerView
        case .Toolbar:
            let toolbarUIPickerView = ToolbarUIPickerView()
            keepPickerviews.append(toolbarUIPickerView)
            return toolbarUIPickerView
        }
    }
}

protocol LanguageUIPickerViewDelegate {
    func onSelectLanguage();
}

class LanguageUIPickerView: ViewBase, UIPickerViewDelegate, UIPickerViewDataSource {
    /// MARK: - Properties.
    
    var delegate: LanguageUIPickerViewDelegate?
    
    private let languageOptions: [String] = [
        "English".localized,
        "Spanish".localized
    ]
    
    // Is maintained two structures to not transform keys to an array
    // and due to the order is not asserted.
    private let languageOptionsTranslate: [String : String] = [
        "English".localized : "en",
        "Spanish".localized : "es",
        "en" : "English".localized,
        "es" :"Spanish".localized,
    ]
    
    private var uiPickerview: UIPickerView!
    
    /// MARK: - Functions.
    
    override func viewDidLoad() {
        uiPickerview = UIPickerView()
        uiPickerview.delegate = self
        uiPickerview.dataSource = self
        view = uiPickerview
        
        let currentLangName = languageOptionsTranslate[Language.getLang()]!
        let currentLangIndex = languageOptions.firstIndex(of: currentLangName)!
        uiPickerview.selectRow(currentLangIndex, inComponent: 0, animated: false)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languageOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languageOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedLangCode = languageOptionsTranslate[languageOptions[row]]
        Language.setLang(selectedLangCode!)
        delegate?.onSelectLanguage()
    }
}

protocol ToolbarUIPickerViewDelegate {
    var currentEditorToolbar: ToolbarOption { get }
    func onSelectedToolbar(_ toolbarOption: ToolbarOption);
}

class ToolbarUIPickerView: ViewBase, UIPickerViewDelegate, UIPickerViewDataSource {
    /// MARK: - Properties.
    
    var delegate: ToolbarUIPickerViewDelegate?

    private let toolbarOptions = [
        ToolbarOption.standard,
        ToolbarOption.chemistry,
        ToolbarOption.simple
    ]

    private var uiPickerview: UIPickerView!
    
    /// MARK: - Functions.

    override func viewDidLoad() {
        uiPickerview = UIPickerView()
        uiPickerview.delegate = self
        uiPickerview.dataSource = self
        view = uiPickerview
        
        let currentToolbarIndex = toolbarOptions.firstIndex(of: delegate!.currentEditorToolbar)!
        uiPickerview.selectRow(currentToolbarIndex, inComponent: 0, animated: false)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return toolbarOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return toolbarOptions[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let toolbarOption = toolbarOptions[row]
        delegate?.onSelectedToolbar(toolbarOption)
    }
}
