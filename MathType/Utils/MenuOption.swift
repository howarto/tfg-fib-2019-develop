import UIKit

enum MenuOption: CaseIterable {
    case Settings
    case Picture
    
    var description: String {
        switch self {
        case .Settings: return "Settings".localized
        case .Picture: return "Get formula from a picture".localized
        }
    }
    
    var image: UIImage {
        switch self {
        case .Settings: return UIImage(named: "baseline_settings_white_24dp") ?? UIImage()
        case .Picture: return UIImage(named: "icons8-screenshot-filled-50") ?? UIImage()
        }
    }
}
