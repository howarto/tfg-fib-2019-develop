import Foundation
import UIKit

class ToastView: UILabel {
    
    private static var overlayView: UIView!
    private static var backView: UIView!
    private static var label: UILabel!
    
    private static func setup(_ view: UIView, message: String) {

        // View to block interaction
        backView = UIView()
        view.addSubview(backView)
        backView.translatesAutoresizingMaskIntoConstraints = false
        backView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        backView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        overlayView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        overlayView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height - 120)
        overlayView.backgroundColor = ColorPicker.darkGray.uiColor
        overlayView.layer.cornerRadius = 10
        view.addSubview(overlayView)
        
        label = UILabel()
        label.textColor = ColorPicker.white.uiColor
        label.text = message
        label.textAlignment = .center
        overlayView.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalTo: overlayView.heightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: overlayView.leftAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: overlayView.rightAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: overlayView.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: overlayView.centerYAnchor).isActive = true
    }
    
    private static func show(_ view: UIView, message: String, duration: Int) {
        setup(view, message: message)
        
        //Animation
        UIView.animate(withDuration: 1, animations: {
            
            overlayView.alpha = 1
            
        }) { (true) in
            UIView.animate(withDuration: 1, animations: {
                
                overlayView.alpha = 0
                
            }) { (true) in
                UIView.animate(withDuration: 1, animations: {
                    DispatchQueue.main.async(execute: {
                        overlayView.alpha = 0
                        label.removeFromSuperview()
                        overlayView.removeFromSuperview()
                        backView.removeFromSuperview()
                    })
                })
            }
        }
    }
    
    static func short(_ view: UIView, message: String) {
        show(view, message: message, duration: 1)
    }
    
    static func long(_ view: UIView, message: String) {
        show(view, message: message, duration: 2)
    }
}
