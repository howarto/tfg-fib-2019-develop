var editor = null;

function wrs_urlencode(clearString) {
    var output = '';
    var x = 0;
    clearString = clearString.toString();
    var regex = /(^[a-zA-Z0-9_.]*)/;

    var clearString_length =
        typeof clearString.length == 'function'
            ? clearString.length()
            : clearString.length;

    while (x < clearString_length) {
        var match = regex.exec(clearString.substr(x));
        if (match != null && match.length > 1 && match[1] != '') {
            output += match[1];
            x += match[1].length;
        } else {
            var charCode = clearString.charCodeAt(x);
            var hexVal = charCode.toString(16);
            output +=
                '%' + (hexVal.length < 2 ? '0' : '') + hexVal.toUpperCase();
            ++x;
        }
    }

    return output;
}

function wrs_mathmlEntities(mathml) {
    var toReturn = '';

    for (var i = 0; i < mathml.length; ++i) {
        //parsing > 128 characters
        if (mathml.charCodeAt(i) > 128) {
            toReturn += '&#' + mathml.charCodeAt(i) + ';';
        } else {
            toReturn += mathml.charAt(i);
        }
    }

    return toReturn;
}

(function() {
    // These lines are inserted in order to avoid autozoom.
    const meta = document.createElement('meta');
    document.head.appendChild(meta);
    meta.outerHTML =
        '<meta name="viewport" content="width=device-width, user-scalable=no" />';

    // Auxiliar functions.

    // Removes all the children of a specific node.
    function removeChildren(node) {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    }

    // Transform any kind of url to File type.
    function urltoFile(url, filename, mimeType) {
        mimeType = mimeType || (url.match(/^data:([^;]+);/) || '')[1];
        return fetch(url)
            .then(function(res) {
                return res.arrayBuffer();
            })
            .then(function(buf) {
                return new File([buf], filename, { type: mimeType });
            });
    }

    /**
     * Gets MathML from LaTeX string
     * @param {String} latex - LaTeX where get MathML.
     */
    function getMathMLFromLatex(latex) {
        var req = new XMLHttpRequest();
        req.open(
            'POST',
            'https://www.wiris.net/demo/editor/latex2mathml',
            false
        );
        req.setRequestHeader(
            'Content-type',
            'application/x-www-form-urlencoded'
        );
        var params = 'latex=' + encodeURIComponent(latex);
        req.send(params);
        if (req.status != 200) {
            return 'Error generating MathML.';
        }
        return req.responseText;
    }

    /**
     * Recognizes an image from file and set the MathML to the editor.
     * @param {File} file - File containing an image to recognize.
     */
    function recognizeImage(file) {
        // Proxy to bypass CORS.
        var url =
            'https://cors-anywhere.herokuapp.com/https://labs.wiris.com/hand/offline/recognize';
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        xhr.open('POST', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        xhr.addEventListener('readystatechange', function(e) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                let latex = JSON.parse(xhr.response).result.latex;
                editor.setMathML(getMathMLFromLatex(latex));
                window.webkit.messageHandlers.onFinalizeLoad.postMessage({
                    finalizeLoad: true
                });
            } else if (xhr.readyState == 4 && xhr.status != 200) {
                console.error('Request failed');
                window.webkit.messageHandlers.onError.postMessage({
                    error: String(xhr.readyState),
                    message: 'Formula could not be recognized'
                });
            }
        });

        formData.append('img', file);
        xhr.send(formData);
    }

    // Firstly, website content is removed in order
    // to instantiate MathType Editor.
    removeChildren(document.body);

    // Append editor script.
    const script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', 'https://www.wiris.net/demo/editor/editor');

    script.onload = function() {
        // Auxiliar functions.

        // Inserts editor into the current html document.
        function insertEditor() {
            const div = document.createElement('div');
            div.id = 'editorContainer';
            document.body.appendChild(div);
            editor = com.wiris.js.JsEditor.newInstance();
            editor.insertInto(div);
        }

        // Adds event listeners
        function addEventListeners() {
            document.addEventListener('mathtypeGetFormula', () => {
                const mathML = editor.getMathML();
                const mathMLURIEncoded = wrs_urlencode(
                    wrs_mathmlEntities(mathML)
                );
                const formulaSrc = `https://www.wiris.net/demo/editor/render.png?centerbaseline=false&backgroundColor=%23fff&dpi=400&mml=${mathMLURIEncoded}`;
                window.webkit.messageHandlers.onGetFormula.postMessage({
                    src: formulaSrc,
                    mml: mathML
                });
            });

            document.addEventListener('mathtypeGetEditorMode', () => {
                let editorMode = '';
                const handIsVisible =
                    document.querySelector('.wrs_disableHand') != null;
                if (handIsVisible) {
                    editorMode = 'standard';
                }
                window.webkit.messageHandlers.onGetEditorMode.postMessage({
                    editorMode
                });
            });

            document.addEventListener('mathtypeRecognizeImage', event => {
                let base64 = event.detail;
                const file = urltoFile(`data:image/png;base64,${base64}`);
                file.then(value => {
                    recognizeImage(value);
                });
            });

            document.addEventListener('touchstart', () => {
                window.webkit.messageHandlers.onTouchStart.postMessage({
                    touchStart: true
                });
            });
        }

        // Insert editor.
        insertEditor();

        // Add listeners.
        addEventListeners();

        const mainFunction = () => {
            var previewImageElement = document.getElementsByClassName(
                'wrs_previewImage'
            )[0];
            previewImageElement.style.pointerEvents = 'all';

            // Observer to control formula changes.
            const config = {
                attributes: true,
                attributeOldValue: true
            };

            function getMathMLFromSrc(src) {
                let mathML = '';
                // Search MathML. There are two cases:
                // - When it's the last parameter in url.
                // - When it's between other parameters.
                const parameterName = 'mml=';
                const mmlStart = src.indexOf(parameterName);
                if (mmlStart !== -1) {
                    const otherParameter = src.indexOf('&', mmlStart);
                    if (otherParameter === -1) {
                        // There isn't more parameters.
                        mathML = src.substring(mmlStart + parameterName.length);
                    } else {
                        // There are one or more parameters
                        mathML = src.substring(
                            mmlStart + parameterName.length,
                            otherParameter
                        );
                    }
                }

                return mathML;
            }

            // Subscriber function.
            function subscriber(mutations) {
                mutations.forEach(mutation => {
                    // handle mutations here
                    if (
                        mutation.type === 'attributes' &&
                        mutation.attributeName === 'src'
                    ) {
                        const oldSrc = mutation.oldValue;
                        if (oldSrc && !ownMutation) {
                            const oldMathML = getMathMLFromSrc(oldSrc);
                            if (oldMathML) {
                                window.webkit.messageHandlers.onRecentFormulasChange.postMessage(
                                    {
                                        src: `${oldSrc.substring(
                                            0,
                                            oldSrc.lastIndexOf('?')
                                        )}?centerbaseline=false&backgroundColor=%23fff&dpi=400&mml=${oldMathML}`,
                                        mml: decodeURIComponent(oldMathML)
                                    }
                                );
                                var previewImageElement = document.getElementsByClassName(
                                    'wrs_previewImage'
                                )[0];
                                previewImageElement.style.maxHeight = null;
                                previewImageElement.style.maxWidth = null;
                            } else if (!ownMutation) {
                                // When it is the loading icon
                                ownMutation = true;
                                var previewImageElement = document.getElementsByClassName(
                                    'wrs_previewImage'
                                )[0];
                                const srcWithDPIs = `${previewImageElement.src.substring(
                                    0,
                                    previewImageElement.src.lastIndexOf('?')
                                )}?centerbaseline=false&backgroundColor=%23fff&dpi=400&mml=${getMathMLFromSrc(
                                    previewImageElement.src
                                )}`;
                                previewImageElement.src = srcWithDPIs;
                                previewImageElement.style.maxHeight = '200px';
                                previewImageElement.style.maxWidth = '300px';
                            }
                        } else {
                            ownMutation = false;
                        }
                    }
                });
            }

            // Variable to look the difference between own mutations and others.
            let ownMutation = false;
            const observer = new MutationObserver(subscriber);
            observer.observe(previewImageElement, config);

            const controlHandMode = mutations => {
                mutations.forEach(mutation => {
                    if (
                        mutation.type === 'attributes' &&
                        mutation.attributeName === 'class'
                    ) {
                        const attribute = mutation.target.classList.contains(
                            'wrs_disableHand'
                        );
                        const editorMode = attribute ? 'standard' : '';
                        window.webkit.messageHandlers.onEditorModeChange.postMessage(
                            {
                                editorMode
                            }
                        );
                    }
                });
            };

            const handModeObserver = new MutationObserver(controlHandMode);
            handModeObserver.observe(
                document.querySelector('.wrs_editor'),
                config
            );
        };

        const timeOutFunction = () => {
            const previewImageElement = document.getElementsByClassName(
                'wrs_previewImage'
            )[0];

            if (!!previewImageElement) {
                mainFunction();
            }
            else {
                setTimeout(() => {
                    timeOutFunction();
                }, 200);
            }
        };
        // Add styles to preview img to enable Drag&Drop. Due to editor load does not have
        // any promise we have to wait aproximatelly 2s to have the editor loaded.
        setTimeout(() => {
            timeOutFunction();
        }, 200);
    };

    // Append script to the document.
    const head = document.getElementsByTagName('head')[0];
    head.appendChild(script);
})();
