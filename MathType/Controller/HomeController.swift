import UIKit

/// Controller for HomeView.
class HomeController {
    // MARK: - Properties
    
    /// View of the current controller.
    private var homeView: HomeView!
    /// Controller of the browser view inside the Home View.
    private var browserController: BrowserController!
    /// Controller of the menu view inside the Home View.
    private var menuController: MenuController!
    /// Controller of the configuration view inside the Home View.
    private var configurationController: ConfigurationController!
    /// Controller of the image picker view inside the Home View.
    private var imagePickerController: ImagePickerController!
}

extension HomeController: HomeViewDelegate {
    /// Instantiate if it's needed a BrowserController and returns an instance of its view.
    ///
    /// - Returns: Browser view as its superclass ViewBase.
    func getBrowserView() -> ViewBase {
        if browserController == nil {
            browserController = BrowserController()
            browserController.delegate = self
        }
        return browserController.getViewController()
    }
    
    /// Instantiate if it's needed a MenuController and returns an instance of its view.
    ///
    /// - Returns: Menu view as its superclass ViewBase.
    func getMenuView() -> ViewBase {
        if menuController == nil {
            menuController = MenuController()
            menuController.delegate = self
        }
        return menuController.getViewController()
    }
    
    /// Instantiate if it's needed a ConfigurationController and returns an instance of its view.
    ///
    /// - Returns: Configuration view as its superclass ViewBase.
    func getConfigurationView() -> ViewBase {
        if configurationController == nil {
            configurationController = ConfigurationController()
            configurationController.delegate = self
        }
        return configurationController.getViewController()
    }
    
    /// Instantiate if it's needed a ImagePickerController and returns an instance of its view.
    ///
    /// - Returns: ImagePicker view as its superclass ViewBase.
    func getImagePickerView() -> ViewBase {
        if imagePickerController == nil {
            imagePickerController = ImagePickerController()
            imagePickerController.delegate = self
        }
        return imagePickerController.getViewController()
    }
}

extension HomeController: ControllerProtocol {
    func getViewController() -> ViewBase {
        homeView = HomeView()
        homeView.delegate = self
        return homeView
    }
}

extension HomeController: BrowserControllerDelegate {
    func onError(_ error: String, with message: String) {
        homeView.onErrorBrowserView(error, with: message)
    }
    
    func onTouchStart() {
        homeView.closeMenu()
    }
    
    func onFinalizeLoad() {
        homeView.hideLoading()
    }
    
    func onBeginToLoad() {
        homeView.showLoading()
    }
    
    func onEditorModeChange(editorMode: String) {
        if editorMode == "standard" {
            homeView.showFloatButton()
        }
        else {
            homeView.hideFloatButton()
        }
    }
    
    func onGetFormula(formula: Formula) {
        let url = URL(string: formula.src)
        let data = try? Data(contentsOf: url!)
        UIPasteboard.general.image = UIImage(data: data!)
    }
    
    func onNewRecentFormula(formula: Formula) {
        menuController.addRecentFormula(formula: formula)
    }
}

extension HomeController: MenuControllerDelegate {
    func onOptionSelected(forMenuOption menuOption: MenuOption) {
        switch menuOption {
        case .Settings:
            homeView.showConfigurationMenu()
            break
        case .Picture:
            homeView.onRecognizeFormulaInImage()
            break
        }
    }
    
    func onRecentFormulaSelected(formula: Formula) {
        homeView.toogleMenu()
        browserController.setFormula(formula: formula)
    }
}

extension HomeController: ConfigurationControllerDelegate {
    func getEditorToolbar() -> ToolbarOption {
        return browserController.getEditorToolbar()
    }
    
    func onSelectedToolbar(_ toolbarOption: ToolbarOption) {
        browserController.onSelectedToolbar(toolbarOption)
    }
    
    func onSelectedLanguage() {
        menuController.onSelectedLanguage()
    }
    
    func onAcceptConfiguration() {
        homeView.hideConfigurationMenu()
    }
}

extension HomeController: ImagePickerControllerDelegate {
    func needRemove() {
        homeView.didImagePicked()
    }
    
    func onImage(didSelect image: UIImage) {
        browserController.recognize(image: image)
    }
}
