import UIKit

/// Controller for configuration menu.
class ConfigurationController {
    // MARK: - Properties
    
    /// Parent controller.
    var delegate: ConfigurationControllerDelegate!
    /// ConfigurationView instance.
    private var configurationView: ConfigurationView!
}

extension ConfigurationController: ControllerProtocol {
    func getViewController() -> ViewBase {
        configurationView = configurationView ?? ConfigurationView()
        configurationView.delegate = self;
        return configurationView
    }
}

extension ConfigurationController: ConfigurationViewDelegate {
    func onSelectedToolbar(_ toolbarOption: ToolbarOption) {
        delegate?.onSelectedToolbar(toolbarOption)
    }
    
    func onSelectedLanguage() {
        delegate?.onSelectedLanguage()
    }
    
    func onAcceptConfiguration() {
        delegate?.onAcceptConfiguration()
    }
    
    func getEditorToolbar() -> ToolbarOption {
        return delegate.getEditorToolbar()
    }
}
