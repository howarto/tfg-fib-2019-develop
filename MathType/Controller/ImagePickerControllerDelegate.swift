import UIKit

protocol ImagePickerControllerDelegate {
    /// Action to do when image was taken.
    ///
    /// - Parameter image: Image in PNG format.
    func onImage(didSelect image: UIImage)
    
    /// Action called when ImagePicker needs to be removed by the superview.
    func needRemove()
}
