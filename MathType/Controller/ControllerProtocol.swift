import UIKit

/// Protocol that all the controllers need to implement
/// in order to standarize function names.
protocol ControllerProtocol {
    // MARK: - Functions
    
    /// Returns the associated view of the controller.
    ///
    /// - Returns: The view of the controller.
    func getViewController() -> ViewBase
}
