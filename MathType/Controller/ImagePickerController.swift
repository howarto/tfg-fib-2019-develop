import UIKit

/// Controller for configuration menu.
class ImagePickerController {
    // MARK: - Properties
    
    /// Delegate
    var delegate: ImagePickerControllerDelegate!
    /// ImagePickerView instance.
    private var imagePickerView: ImagePickerView!
}

extension ImagePickerController: ControllerProtocol {
    func getViewController() -> ViewBase {
        imagePickerView = imagePickerView ?? ImagePickerView()
        imagePickerView.delegate = self;
        return imagePickerView
    }
}

extension ImagePickerController: ImagePickerViewDelegate {
    func needRemove() {
        delegate?.needRemove()
    }
    
    func onImage(didSelect image: UIImage) {
        delegate?.onImage(didSelect: image)
    }
}
