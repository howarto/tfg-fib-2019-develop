import UIKit

protocol BrowserControllerDelegate {
    /// Function event called when a new recent formula is created.
    ///
    /// - Parameter formula: New formula created recently.
    func onNewRecentFormula(formula: Formula)
    
    /// Function event called when get the current formula.
    ///
    /// - Parameter formula: The current formula.
    func onGetFormula(formula: Formula)
    
    /// Function event called when editor mode is changed.
    ///
    /// - Parameter editorMode: The current editor mode.
    func onEditorModeChange(editorMode: String)
    
    /// Function event called when something began to load something.
    func onBeginToLoad()
    
    /// Function event called when something finalizes to load something.
    func onFinalizeLoad()
    
    /// Function event called when touch is started in Editor.
    func onTouchStart()
    
    /// Function event called when there is an error is started in Editor.
    ///
    /// - Parameters:
    ///   - error: Error id.
    ///   - message: Message related to error.
    func onError(_ error: String, with message: String)
}
