import UIKit

/// Controller for MenuView.
class MenuController {
    // MARK: - Properties
    
    private var menuView: MenuView!
    var delegate: MenuControllerDelegate?
    
    // MARK: - Functions
    
    /// Adds a new recent formulas in MathML format.
    ///
    /// - Parameter formula: Formula in MathML format.
    func addRecentFormula(formula: Formula) {
        menuView.addRecentFormula(formula: formula)
    }
    
    /// Function that must be called after change language.
    func onSelectedLanguage() {
        menuView.onSelectedLanguage()
    }
}

extension MenuController: ControllerProtocol {
    func getViewController() -> ViewBase {
        if menuView == nil {
            menuView = MenuView()
            menuView.delegate = self
        }
        return menuView
    }
}

extension MenuController: MenuViewDelegate {
    func onOptionSelected(forMenuOption menuOption: MenuOption) {
        delegate?.onOptionSelected(forMenuOption: menuOption)
    }
    
    func onRecentFormulaSelected(formula: Formula) {
        delegate?.onRecentFormulaSelected(formula: formula)
    }
}
