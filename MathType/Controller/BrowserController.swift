import UIKit

/// Controller for BrowserView.
class BrowserController {
    // MARK: - Properties
    
    /// Parent controller.
    var delegate: BrowserControllerDelegate?
    /// BrowserView instance.
    private var browserView: BrowserView!
    
    /// Calls the view to insert a formula into the editor.
    ///
    /// - Parameter formula: Formula to insert.
    func setFormula(formula: Formula) {
        browserView.setFormula(formula: formula)
    }
    
    /// Executed actions after toolbar change event.
    ///
    /// - Parameter toolbarOption: Chosen toolbar.
    func onSelectedToolbar(_ toolbarOption: ToolbarOption) {
        browserView.setToolbar(toolbarOption)
    }
    
    /// Gets the current toolbar option of the editor.
    ///
    /// - Returns: The current toolbar option of the editor.
    func getEditorToolbar() -> ToolbarOption {
        return browserView.getEditorToolbar()
    }
    
    /// Recognize formula in image and set the image in editor.
    ///
    /// - Parameter image: An image.
    func recognize(image: UIImage) {
        let base64Image = image.pngData()?.base64EncodedString()
        browserView.recognizeImage(with: base64Image!)
    }
}

extension BrowserController: ControllerProtocol {
    func getViewController() -> ViewBase {
        browserView = browserView ?? BrowserView()
        browserView.delegate = self;
        return browserView
    }
}

extension BrowserController: BrowserViewDelegate {
    func onError(_ error: String, with message: String) {
        delegate?.onError(error, with: message)
    }
    
    func onTouchStart() {
        delegate?.onTouchStart()
    }
    
    func onFinalizeLoad() {
        delegate?.onFinalizeLoad()
    }
    
    func onBeginToLoad() {
        delegate?.onBeginToLoad()
    }
    
    func onEditorModeChange(editorMode: String) {
        delegate?.onEditorModeChange(editorMode: editorMode)
    }
    
    func onGetFormula(formula: Formula) {
        delegate?.onGetFormula(formula: formula)
    }
    
    func onNewRecentFormula(formula: Formula) {
        delegate?.onNewRecentFormula(formula: formula)
    }
}
