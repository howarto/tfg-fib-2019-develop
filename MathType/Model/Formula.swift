class Formula {
    var src: String!
    var mml: String!
    
    init(from src: String, withMathML mml: String) {
        self.src = src
        self.mml = mml
    }
}
