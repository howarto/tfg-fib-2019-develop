enum ToolbarOption {
    case standard
    case chemistry
    case simple
    
    var name: String {
        switch self {
        case .standard:
            return "Standard".localized
        case .chemistry:
            return "Chemistry".localized
        case .simple:
            return "Simple".localized
        }
    }
    
    var internalEditorName: String {
        switch self {
        case .standard:
            return "general"
        case .chemistry:
            return "chemistry"
        case .simple:
            return "quizzes"
        }
    }
}
