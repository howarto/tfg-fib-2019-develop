BUILD_TYPE=Release
TARGET_NAME=MathType
CPU_ARCHITECTURE=x86_64
SIMULATOR_OR_IOS_SDK=iphonesimulator12.1

xcodebuild -configuration ${BUILD_TYPE} -target ${TARGET_NAME} -arch ${CPU_ARCHITECTURE} -sdk ${SIMULATOR_OR_IOS_SDK} || exit 1
